# -*- coding: utf-8 -*-

from db.models import *
from pydantic import BaseModel, constr
from typing import Optional


class SettingsModel(BaseModel):
    id: int
    name: Optional[str]
    value: Optional[str] = None

    class Config:
        orm_mode = True


