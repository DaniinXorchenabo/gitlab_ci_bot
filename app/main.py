# -*- coding: utf-8 -*-

import uvicorn
from typing import Optional
from db.models import *
from fastapi import FastAPI


app = FastAPI()


@app.get("/")
def read_root():
    return {"Привет!": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Optional[str] = None):
    return {"item_id": item_id, "q": q}

if __name__ == "__main__":

    a = set(dir())
    def ggg(pi1='988888888888888'):
        from math import pi
        ae = '---'
        print()
        print('---')

    ggg()
    print(set(dir()) - a)
    from inspect import getsource
    code = getsource(ggg).split('\n')
    count_tabs = code[0].split('def')[0].count(' ')
    code = "\n".join([''.join(list(i)[count_tabs + 1:]) for i in code[1:]])
    print(code)
    print(count_tabs)
    # string = """print("hi");print("world")"""
    # eval(string)
    # uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
    import subprocess
    import os
    pid = str(os.getpid())
    # returned_output = subprocess.check_output(["tasklist"], shell=True,).decode('utf-8', errors='ignore' )  # returned_output содержит вывод в виде строки байтов
    # returned_output = [os.system(f"Taskkill /PID {i[1]} /F") for i in filter(lambda i: len(i) == 2 and i[0] == 'python.exe' and str(i[1]) != str(pid), [i.strip().split()[:2] for i in returned_output.split('\n')])]
    # subprocess.Popen()
    # print(returned_output)
    # print('Результат выполнения команды:', *returned_output, sep='\n')  # Преобразуем байты в строку
    # while True:
    #     pass

    te = """
from flask import Flask, request, json
if __name__ == "__main__":
    from multiprocessing import Pool, cpu_count, Manager
    
    pool = Pool(processes=4)
app1 = Flask(__name__)
def fff():
    pass
sas = lambda j: j+ 1
sas1 = 456
if 1 == 1:
    app1 = None
else:
    app1 = app1
"""
    # app1 = Flask(__name__)
    exec(te)
    print(app1)

# setattr(ggg, "sss", 234)
# print(ggg())

# from time import time
# from itertools import chain
#
# start_time = time()
# end_iter = iter([])
# def generator():
#     import sys
#     live = True
#     while live:
#         for i in chain(end_iter, sys.stdin):
#             if i == 'end.':
#                 live = False
#             print('**')
#             yield i
#             print("***")
#         # print('---')
#
#
# for i in generator():
#     print(i)
#     if time() - start_time > 20:
#         end_iter = iter(['end.'])
#         # print('***')
#
# print('88888')





'''
class Stone:
    mass = 0;
    color = (0, 0 ,0) ; # RGB
    form = None;
    count = 0

    def __init__(self, my_mass=0,  color=(0,0,0), form="rectangle", q=1, forse=0,   *other_params, Worldview='Атеизм', **kwargs):
        self.mass = my_mass
        self.color = color
        self.form = form
        self.q = q
        self.other_params = other_params
        self.other_params_with_names = kwargs
        self.forse = forse
        self.worldview = Worldview
        Stone.count += 1

    def add_forse(self, add_f):
        self.forse += add_f

    def __add__(self, other_stone):
        self.mass += other_stone.mass
        self.color = tuple([(i + j)/2 for i, j in zip(self.color, other_stone.color)])
        self.form = self.form + ' + ' + other_stone.form
        self.worldview = self.worldview + " с наклонностями к "  + other_stone.worldview
        del other_stone
        return self

print(Stone.count)







d = """ ------  """
f = """  =-=-  """
# df = f'{d}, 888*xufcvjxodfnvkx, \n{" - ".join([str(i) for i in range(100)])}  '
# print(df)

stone1 = Stone(1);
stone2 = Stone(2.666, (255, 0 ,0), "right", 5, 4,5,6,7,8, v=4, d=3, size=(3,3,3), Worldview='пастофарианство');
stone3 = Stone();
stone4 = Stone(q=4, color=(3,3,3));
print(stone1.__dict__)
print(stone2.__dict__)
print(stone3.__dict__)
print(stone4.__dict__)

stone5 = stone1 + stone2
print(stone1.__dict__)
print(stone5.__dict__)
print(Stone.count)
print(stone5.count)
print(stone3.count)
'''



























